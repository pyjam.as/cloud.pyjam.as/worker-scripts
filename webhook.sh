#!/bin/bash
sudo docker inspect $(sudo docker run --security-opt seccomp=unconfined --net=pyjamdhcp -d registry.gitlab.com/pyjam.as/cloud.pyjam.as/base-docker-image) | jq -r .[].NetworkSettings.Networks.pyjamdhcp.IPAddress
