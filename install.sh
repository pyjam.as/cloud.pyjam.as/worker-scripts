set -e
apt install -y webhook dnsutils jq
echo '*  *    * * *   root    curl "http://pyjam.as:8000/$(curl https://ip4.tyk.nu/)"' > /etc/crontab
echo '*  *    * * *   root    dig legacy-$(echo $(curl https://ip4.tyk.nu/) | sed "s/\./-/g").cloud.pyjam.as txt' >> /etc/crontab
cp ./pyjamas-dhcp.service /etc/systemd/system/pyjamas-dhcp.service
cp ./webhook.service /etc/systemd/system/webhook.service
systemctl disable dhcpcd.service
systemctl disable networking.service
systemctl enable pyjamas-dhcp.service
systemctl enable webhook.service

# this can fail but it's the last thing so it doesn't block 💩
docker plugin install --grant-all-permissions ghcr.io/devplayer0/docker-net-dhcp:latest-linux-arm-v7
