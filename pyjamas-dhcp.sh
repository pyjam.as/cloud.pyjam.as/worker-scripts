#!/usr/bin/env bash

# make bridge interface
ip link add my-bridge type bridge
ip link set my-bridge up

# start eth0 shit
ip link set eth0 up
ip link set eth0 master my-bridge
iptables -A FORWARD -i my-bridge -j ACCEPT
dhcpcd my-bridge

# make the docker network

if [ $(docker network ls | grep pyjamdhcp | wc -l) != 1 ]; then
	echo "making docker network"
	docker network create -d ghcr.io/devplayer0/docker-net-dhcp:latest-linux-arm-v7 --ipam-driver null -o bridge=my-bridge pyjamdhcp
else
	echo "not making network"
fi
